var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017/test';


MongoClient.connect(url, function (err, db) {
  if(err) {
    console.log('Невозможно подключиться к базе данных MongoDB. Ошибка ', err)
  } else {
    console.log('Соединение установлено для ', url);

    var collection = db.collection('users');

    const user1 = {name: 'Anya', gender: 'f'};
    const user2 = {name: 'Katya', gender: 'f'};
    const user3 = {name: 'Dron', gender: 'm'};

    collection.insert([user1, user2, user3]);

    var cursor = collection.find();
    showUsers();

    collection.update({gender: 'm'}, {$set: {name: 'Alex'}});
    showUsers();
    collection.remove();
    showUsers();

    db.close();

    function showUsers() {
      collection.find().toArray(function (err, result) {
        if(err) {
          console.log(err);
        } else if(result.length) {
          console.log(result);
        } else {
          console.log('Нет документов с данным условием поиска.');
        }
      });
    }
  }
});
